def call() {
    emailext (
        subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: """FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'\r\nCheck console output at: \r\n"${env.BUILD_URL}\r\nPlease check out attachment for more log details""",
        attachLog: true,
        to: "${env.gitlabUserEmail}"
    )
}
