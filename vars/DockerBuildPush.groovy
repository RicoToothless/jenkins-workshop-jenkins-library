def call() {
    container('eks-helmsman') {
        env.ECR_IMAGE_URI = sh (script: "aws ecr describe-repositories --repository-names ${SERVICE_NAME} | grep repositoryUri | cut -d '\"' -s -f4", returnStdout: true).trim()
        sh "aws ecr get-login --no-include-email | sed 's|https://||' > docker-ecr-login.sh"
        sh "chmod +x docker-ecr-login.sh"
    }
    container('docker') {
        sh "ash docker-ecr-login.sh"
        sh "docker build -t ${ECR_IMAGE_URI}:${PACKAGE_VERSION} ."
        sh "docker push ${ECR_IMAGE_URI}:${PACKAGE_VERSION}"
    }
}
