def call() {
    container('eks-helmsman') {
        sh """
        sed -i -e "s~{{IMAGE_WILL_BE_FILLED_DYNAMICALLY}}~${ECR_IMAGE_URI}~g" helm-chart/values.yaml
        sed -i -e "s~{{TAG_WILL_BE_FILLED_DYNAMICALLY}}~${PACKAGE_VERSION}~g" helm-chart/values.yaml
        helm upgrade --namespace default --install --recreate-pods ${SERVICE_NAME} ./helm-chart -f helm-chart/values.yaml
        """
    }
}
