def call() {
    if(!(env.PACKAGE_VERSION ==~ /\d+\.\d+\.\d+/)) {
        error "Package version ${PACKAGE_VERSION} is not a valid version number. (Expect x.y.z)"
    }
}
